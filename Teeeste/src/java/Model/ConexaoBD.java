package Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexaoBD {
private String caminho = "jdbc:postgresql://localhost:5432/bd";
    private String driver = "org.postgresql.Driver";
    private String usuario = "postgres";
    private String senha = "amanda123";
    
    public Connection getConnection()throws ClassNotFoundException{
        try{
            Class.forName(driver);
            return DriverManager.getConnection(caminho, usuario, senha);
            
        }catch (SQLException e){
            System.out.println("Erro no banco de dados");
         throw new RuntimeException(e);   
        }
    } 
}
    

