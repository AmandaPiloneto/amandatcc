package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LivrosDAO {
 private Connection con;

public LivrosDAO()throws ClassNotFoundException{
    try{
        this.con=new ConexaoBD().getConnection();
    }catch(ClassNotFoundException e){
        System.out.println(e.getMessage());
    }
}

public void Cadastra (Livros liv){
    
    String sql="Insert into livros (Titulo,Autor,Valor)"
            +"values (?,?,?)";
    
    
    
    try{
        PreparedStatement st=con.prepareStatement(sql);
        
        st.setString(1, liv.getTitulo());
        st.setString(2, liv.getAutor());
        st.setDouble(3, liv.getValor());
        
        st.execute();
        con.close();
    }catch(SQLException e){
        throw new RuntimeException(e);
    }
}

public List<Livros> consulta(){
    
    try{
        //Cria um ArrayList para receber os carros cadastrados
        List<Livros> livros = new ArrayList<Livros>();
        PreparedStatement st = this.con.prepareStatement("SELECT * FROM livros");
        //Executa uma consulta
        ResultSet rs = st.executeQuery();
        
        while (rs.next()){
            //Percorre linha por linha para preencher o objeto
            Livros liv = new Livros();
            liv.setTitulo(rs.getString("Titulo"));
            liv.setAutor(rs.getString("Autor"));
            liv.setValor(rs.getDouble("Valor"));
            
            Livros.add(liv);
        }
        rs.close();
        st.close();
        List<Livros> Livros = null;
        
        //Mostra o ArrayList depois de receber os usuarios do bd
        return Livros;
        
    } catch(SQLException e){
        throw new RuntimeException(e);
        
            
        }
    }

}