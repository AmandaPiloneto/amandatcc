package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UsuarioDAO {


public void Cadastra (Usuario usu){
    
    String sql="Insert into usuario (nome,email,senha,cpf,cep,endereco,estado,cidade)"
            +"values (?,?,?,?,?,?,?,?)";
    
    
    
    try{
        Connection con = ConexaoBD.getConnection();
        PreparedStatement st=con.prepareStatement(sql);
        
        st.setString(1, usu.getNome());
        st.setString(2, usu.getEmail());
        st.setString(3, usu.getSenha());
        st.setInt(4, usu.getCpf());
        st.setInt(5, usu.getCep());
        st.setString(6, usu.getEndereco());
        st.setString(7, usu.getEstado());
        st.setString(8, usu.getCidade());
        
        st.execute();
        con.close();
    }catch(SQLException e){
        throw new RuntimeException(e);
    }
}

public boolean getUsuario(String email, String senha){
        boolean ext = false;
        String sql = "select * from usuario where email=? and senha=? ";
        try{
            con = ConexaoBD.getConnection();
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, email);
            stmt.setString(2, senha);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                ext = true;
                System.out.print("Existe");
            }else{
                System.out.print("Nao existe");
            }
        }catch(SQLException e){
            System.out.print(e.getMessage());
        }
        return ext;
        }

 
}




