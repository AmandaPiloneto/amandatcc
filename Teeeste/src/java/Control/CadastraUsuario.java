package Control;

import Model.Usuario;
import Model.UsuarioDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "CadastraUsuario", urlPatterns = {"/CadastraUsuario"})
public class CadastraUsuario extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            //Busca o que foi digitado na JSP através do requst.getParameter()
            String Nome = request.getParameter("nome");
            String Email = request.getParameter("email");
            String Senha = request.getParameter("senha");
            String CPF = request.getParameter("cpf");
            String CEP = request.getParameter("cep");
            String Endereco = request.getParameter("endereco");
            String Estado = request.getParameter("estado");
            String Cidade = request.getParameter("cidade");
            
            Usuario usu = new Usuario();
            usu.setNome(Nome);
            usu.setEmail(Email);
            usu.setSenha(Senha);
            usu.setCpf(Integer.valueOf(CPF));
            usu.setCep(Integer.valueOf(CEP));
            usu.setEndereco(Endereco);
            usu.setEstado(Estado);
            usu.setCidade(Cidade);
            
            try{
                //Salva o cliente
            UsuarioDAO dao = new UsuarioDAO();
            dao.Cadastra(usu);
            }catch (ClassNotFoundException ex){
                
            }
            
            RequestDispatcher rd = request.getRequestDispatcher("index.html");
            rd.forward(request, response);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

