package Teste;

import Model.ConexaoBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TestaConexao {
    public static void main (String[] args) throws SQLException {
        Connection con = null;
        
        try{
            con = new ConexaoBD().getConnection();
            System.out.println("Conexão aberta");
            
            String sql="Insert into usuario(nome,email,senha,cpf,cep,endereco,estado,cidade)"
                    +"values(?,?,?,?,?,?,?,?)";
            
            PreparedStatement st = con.prepareStatement(sql);
            
            st.setString(1, "Amanda");
            st.setString(2, "amanda.piloneto@gmail.com");
            st.setString(3, "cereja");
            st.setInt(4, 1234567890);
            st.setInt(5, 83430000);
            st.setString(6, "Rua dos Prazeres");
            st.setString(7, "Paraná");
            st.setString(8, "Curitiba");
            
            st.execute();
            
            System.out.println("Usuário cadastrado com sucesso");
            
        }catch (Exception e){
            throw new RuntimeException(e);
        }finally{
            con.close();
        }
    }
}


