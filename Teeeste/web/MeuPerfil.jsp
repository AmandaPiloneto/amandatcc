<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.min.css" rel="stylesheet"/>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
   
    <link rel="stylesheet" href="./css/aviso.css"/>
    <link rel="stylesheet" href="./css/estilo.css"/>
    <link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css"/>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" 
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">P�gina inicial</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="CadastraUsuario.jsp">Ainda n�o tem uma conta?</a>
                    </li>
                    <li>
                        <a href="sobrenos.html">Sobre n�s</a>
                    </li>
                    <li>
                        <a href="MeuPerfil.jsp">Meu perfil</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">
    
        <div class="col-md-12"> 
            
               <section id="conteudoLogin">
                <div class="row col-md-4 col-md-offset-4 conteudoLogin">
                    <form class="login" action="validaLogin" method="post">
                        
                        <br><br>
                        <br><br>
                        <br><br>
                        <br><br>
                        
                        <h2 class="tituloLogin">Bem-Vindo</h2>
                       
                        <br><br>
                        
                        <input style='width:250px' name="nEmail"  type="email" id="email" class="form-control" placeholder="Digite seu e-mail" required autofocus>
                        <br>
                        <input style='width:250px' name="nSenha" type="password" id="senhaLogin" class="form-control" placeholder="Digite sua senha" required>
                        <%
                            try{
                                String msg = request.getAttribute("loginError").toString();
                                out.print("<div class='alert alert-danger loginAviso'><strong>AVISO! </strong>"+msg+"</div>");
                            }catch(Exception e){
                                System.out.print(e.getMessage());
                            }
                        %>
                        <br>
                        <button style='width:80px; position:relative;line-height: 25px;' type="submit" id="logarLogin" class="btn btn-sm btn-success">Logar</button>
                    </form>
                </div>
            </section>    
        </div>    
    

       