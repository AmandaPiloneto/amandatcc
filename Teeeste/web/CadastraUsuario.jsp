<%-- 
    Document   : CadastraUsuario
    Created on : 18/06/2016, 21:53:53
    Author     : Tiago
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
   

<head>
    
    <style rel="stylesheet">
        .botao01{
            background: -webkit-linear-gradient(bottom, #E0E0E0, #F9F9F9 70%);
                 background: -moz-linear-gradient(bottom, #E0E0E0, #F9F9F9 70%);
                 background: -o-linear-gradient(bottom, #E0E0E0, #F9F9F9 70%);
                 background: -ms-linear-gradient(bottom, #E0E0E0, #F9F9F9 70%);
                 background: linear-gradient(bottom, #E0E0E0, #F9F9F9 70%);
                 border: 1px solid #CCCCCE; 
                 border-radius: 3px;
                 box-shadow: 0 3px 0 rgba(0, 0, 0, .3), 0 2px 7px rgba(0, 0, 0, 0.2);
                 color: #616165;
                 display: block;
                 font-family: "Trebuchet MS";
                 font-size: 14px;
                 font-weight: bold;
                 line-height: 25px;
                 text-align: center;
                 text-decoration: none;
                 text-transform: uppercase;
                 text-shadow:1px 1px 0 #FFF;
                 padding: 5px 15px;
                 position: relative;
                 width: 20%; }
        
        .botao01:before{
            border: 1px solid #FFF;
            border-radius: 3px;
            box-shadow: inset 0 -2px 12px -4px rgba(70, 70, 70, .2), 
                inset 0 3px 2px -1px rgba(255, 255, 255, 1); content: "";
            bottom: 0; left: 0; right: 0; top: 0;
            padding: 5px;
            position: absolute;
        }
        
        .botao01:after{
            background: rgba(255, 255, 255, .4);
            border-radius: 2px; content: "";
            bottom: 15px; left: 0px;
            right: 0px; top: 0px;
            position: absolute;
        }
        
        .botao01:active{
            box-shadow: inset 0 0 7px rgba(0, 0, 0, .2);
            top: 4px;
        } 
        
        .botao01:active:before{
            border: none;
            box-shadow:none;
        }
        
    </style>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Spectrum</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/shop-homepage.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="style.css">

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" 
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Página inicial</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#">Ainda não tem uma conta?</a>
                    </li>
                    <li>
                        <a href="sobrenos.html">Sobre nós</a>
                    </li>
                    <li>
                        <a href="MeuPerfil.jsp">Meu perfil</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead" style='font-family: Pristina;
                   font-size:72px;
                   text-shadow: 1px 1px 1px black;'>Spectrum</p>
                
                <div class="list-group">
                    <a href="TodosProdut.jsp" class="list-group-item">Ver todos os produtos</a>
                </div>
            </div>

            <div class="col-md-9">

                <div class="row carousel-holder">

                    
                    <div>
                    
                    	<h1 class="lead" style='font-family:Pristina ;
                            text-align: center;
                            font-size:72px;
                            text-shadow: 1px 1px 1px black;'>Cadastre-se</h1>

                            <br>
                            <br>
                            <br>
                          <center>
                               <form action="CadastraUsuario" method="post">
                                   <a style='text-align: center; 
                            text-shadow: 1px 1px 1px black;'>Nome: <input style='color: black' class="form-control" type="text" name="nome"/></a><br/>
                                
                                <a style='text-align: center;
                            color: #000; 
                            text-shadow: 1px 1px 1px black;'>Email: <input style='color: black' class="form-control" type="text" name="email="/></a><br/>
                                
                                <a style='text-align: center;
                            color: #000; 
                            text-shadow: 1px 1px 1px black;'>Senha: <input style='color: black' class="form-control" type="password"name="senha"/><a/><br/>
                                
                                <a style='text-align: center;
                            color: #000; 
                            text-shadow: 1px 1px 1px black;'>CPF: <input style='color: black' class="form-control" type="text"name="cpf"/></a><br/>
                                
                                <a style='text-align: center;
                            color: #000; 
                            text-shadow: 1px 1px 1px black;'>CEP: <input style='color: black' class="form-control" type="text"name="cep" /></a><br />
                                
                                <a style='text-align: center;
                            color: #000; 
                            text-shadow: 1px 1px 1px black;'>Endereço: <input style='color: black' class="form-control" type="text"name="endereco"/></a><br />
                                
                                <a style='text-align: center;
                            color: #000; 
                            text-shadow: 1px 1px 1px black;'>Estado:<input style='color: black' class="form-control" type="text"name="estado"/></a><br />
                                
                                <a style='text-align: center;
                            color: #000; 
                            text-shadow: 1px 1px 1px black;
                            '>Cidade: <input style='color: black' class="form-control" type="text"name="cidade"/></a><br />
                                

                                <input type="submit" value="Cadastrar" class="botao01" />

                           </form>
                           </center>
                    </div>
                


            </div>

        </div>

    </div>
    <!-- /.container -->

    <div class="container">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p style='text-align: center;
                       font-size: 23px;
                       color: #000; 
                       text-shadow: 1px 1px 1px black;'>Copyright Amanda, Nicolas e Tiago</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
        
        
    